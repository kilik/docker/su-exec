# su-exec

Binary package of su-exec [see official repository https://github.com/ncopa/su-exec](https://github.com/ncopa/su-exec)

## work on this project

```shell
cd ~/docker
git clone git@gitlab.com:kilik/docker/su-exec.git
cd su-exec
docker image build -t su-exec .
```

## publish this image

```shell
docker image build -t kilik/su-exec .
docker image push kilik/su-exec
```
