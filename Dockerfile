FROM debian:bullseye-slim AS builder

ENV DEBIAN_FRONTEND noninteractive

# install dev tools and compiler
RUN apt-get update \
    && apt-get install  -y \
        git \
        make \
        gcc

# clone projet and build binary
RUN git clone https://github.com/ncopa/su-exec.git && \
    cd su-exec && \
    make

# build final image / package
FROM scratch
COPY --from=builder /su-exec/su-exec /
